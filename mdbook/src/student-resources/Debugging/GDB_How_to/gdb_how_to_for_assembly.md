# GDB How to for assembly

After you have compiled and linked your assembly file 

***Note*** there are hello world assembly files in the student resources folder:
https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/tree/master/mdbook/src/student-resources/hello_world
* One for 32 bit registers (one_hello_world_32.asm) 
* One for 64 bit registers (two_hello_world_64.asm).  



| |
|-|
|Example of compiling and linking and running asm|
|![Compile,link,run](./assets/01_compile_link_run.png)|

This file shows three ways to use gdb to debug. 

- One: with just the gdb commands
- Two: with the tui inside gdb
- Three: with a gdbinit file that displays the registers and stack

---
## One: Just GDB commands

| |      
|-|
|Type in the command **gdb** followed by the executable file name 
|![gdb_inital](./assets/02_gdb_command.png)|
|The results will look similar to the following (the executable name may not match what is show here)|
|![gdb_results](./assets/03_gdb.png)|
|To see the help commands type: **help**|
|![gdb_help](./assets/04_gdb_help.png)|
|For specifics in help, type a keyword after the help command. For example, **help step**|
|![gdb__help_specifics](./assets/05_help_specifics.png)|
|We begin debugging by entering a break point. We will use **break _start**.  We can also use labels and functions.|
|![break_point](./assets/06_break_point.png)|
|Now we **run** |![run](./assets/07_run.png)|
|We can use the **disassemble** command to see the registers and their values of our assembly code.|
|![disassemble](./assets/08_disassemble.png)|
|Notice that this is showing AT&T syntax.  To see intel syntax use **set disassembly-flavor intel** |
|![flavor_intel](./assets/08_disassemble.png)|
|Running the **disassemble** command again shows intel syntax of the assembly code.|
|![intel_disassemble](./assets/10_intel_disassemble.png)|
|To step into the next line of code use the command **stepi**|
|![stepi](./assets/11_stepi.png)|
|To see all the registers use the command **info registers**|
|![info_registers](./assets/12_info_registers.png)|
|Enter the command **stepi** again to step into the next line. Then run **info registers** again to see the changes to the registers.Continue using **stepi**, followed by **info registers** to see the changes in the registers.
|![step_info_registers](./assets/13_stepi_info_registers.png)|
|When you are done debugging use the **quit** command|
|![quit](./assets/14_quit.png)|

***Try looking up and experimenting with other gdb commands***

## TWO: With TUI (Terminal user interface)
| | |
|-|-|
|To use TUI (Terminal user interface) Use the **gdb** command followed by the name of the executable you want to debug|
|![gdb_helloworld](./assets/15_gdb_helloworld.png)|
|Now enter **tui enable**|
|![tui](./assets/16_tui.png)|
|Your screen should look like the image below|
|![tui_initial](./assets/17_tui_initial.png)|
|Now we are going to set the syntax to intel with the command **set disassembly-flavor intel**|
|![flavor_intel](./assets/18_flavor_intel.png)|
|Set a break point|
|![break](./assets/06_break_point.png)|
|To see the registers in the tui run the command **layout asm**|
|![layout_asm](./assets/19_layout_asm.png)|
|Then enter the command **run**|
|![tui_run](./assets/20_tui_run.png)|
|To see the registers in our program run the command **layout reg**|
|![layout_reg](./assets/21_layout_reg.png)|
|You should see something similar on your screen|
|![tui_layout_reg](./assets/22_tui_layout_reg.png)|
|Now we can begin stepping through the code with **stepi**. The screen will update with the values placed in the registers|
|![tui_step](./assets/23_tui_step.png)|
|To end the gdb use the quit command|
|![tui_step](./assets/14_quit.png)|

## THREE: With GDBINIT

This involves using a gdbinit file that will format and launch the tui automatically to show the registers and stack at gdb startup.

In gitlab you will find the directory and files to clone here:

  https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/tree/master/mdbook/src/student-resources/Debugging.

Clone the ConfigFile directory and it’s files.
There are directions in the Install_gdbinit.txt

This is what it says: 

    - From the ConfigFiles directory in your terminal
    (cd into the directory 
    Example: cd Documents/Assembly/ConfigFiles)
    - Run the following command to create a display 
    of the registers when running gdb.
    It will create a gdbinit in your home directory

        cp ./gdbinit ~/.gdbinit


    Note: in the gdbinit file, you can change
    to show either the 64 or 32 registers by
    going to the gdb options section and
    change: 
    set $64BITS = 0 (for 32 bit registers)
    set $64BITS = 1 (for 64 bit registers)

Once you have the gdbinit copied to your home directory verify by cd-ing into your home directory and typing the command “ls –a”
You should see it 

![verify_gdbinit](./assets/24_verify_gdbinit.png)

From here you will debug as you would in the TWO: With TUI (Terminal user interface) section.

You will be able to see items pushed and popped on to the stack when the push and pop commands are used.
You will not need to enter the assembly layout or intel flavor because it is preset in the gdbinit. 

Now we will walk through Hello world in 32 bit registers with gdbinit. 

***Note: to run the two_hello_world_64.asm with gdbinit, you will need to follow the directions in the 
ConfigFiles directory, the file named Install_gdbinit.txt.  The gdbinit file must be modified and recopied to the home directory.***

___
## one_hello_world_32.asm

![hello-world_code](./assets/25_hello_world_code.png)


### GDB Commands
||
|-|
|To start debugging the file after it’s been compiled and linked to create the executable|
|![hello-world_code](./assets/26_gdb_one_hello_world_32.png)|
|Now we are going to set a break point at the beginning of the assembly program at the **_start**. If we had a function or label we wanted to start at we would put that after the **break** instead of “**_start**”|
|![hello-world_code](./assets/06_break_point.png)|
|To begin looking at the code we need to **run** the program, it will run and break at **_start**|
|![hello-world_code](./assets/07_run.png)|
|Because of the gdbinit file we used, the tui should automatically launch. We will use the command **stepi** to step into the next section of code. **mov edx, 0xd** is what  **mov edx, len** in our code is translated to.|
|![initial](./assets/27_initial.png)
|Notice that after we hit enter that the line **mov edx, oxd** is no longer visible. If you look at the **EDX** register that is circled in red, you can see that it now has **D** which is hex for 13, which is the length of the **msg** **'Hello World!’, 0xa**. To step into the next section we again enter **stepi**|![step1](./assets/28_step1.png)|
|Now the command **mov ecx, 0x804a000** has moved off which was what our code **mov ecx, msg** was translated to. We can see that the **ECX** register now has that value. We will continue stepping with the **stepi** command| 
|![step2](./assets/29_step2.png)|
|Now the command **mov ebx, 0x1** has moved off which is what our code **mov ebx, 1** was translated to, which is the code for STDOUT. Again it is circled in red. Continue with **stepi**.| 
|![step3](./assets/30_step3.png)|
|Now the command **mov eax,0x4** has moved off which is what our code **mov eax,4** was translated to, which is the code for invoking sys_write. Again it is circled in red. Continue with **stepi**.|
|![step4](./assets/31_step4.png)|
|Now the command **int 0x80** has moved off which is how we invoke the kernel interrupt. Now “Hello World!” is printed to STDOUT. Again it is circled in red. Continue with **stepi**.|
|![step5](./assets/32_step5.png)|
|Now the command **mov ebx, 0x0** has moved off which is what our code **mov ebx, 0** is translated to, which is the code for having a return status of “no errors”. Again it is circled in red. Continue with **stepi**.|
|![step6](./assets/33_step_6.png)|
|Now the command **mov eax, 0x1** has moved off which is what our code **mov eax,1** was translated to, which is the code for invoking sys_exit. Again it is circled in red. Continue with **stepi**.|
|![step7](./assets/34_step7.png)|
|Now that the code as exited we see that it has exited normally. To exit gdb we can quit with the command **quit**|
|![step8](./assets/35_step8.png)|

---
Now go back and change the ***gdbinit*** to work with the 64 bit register. (You may need to refer to the directions in the install_gdbinit.txt in the ConfigFile directory.
)
Compile, link and run the two_hello_world_64.asm file. 
Once you have it working, debug with gdb and step through. 

Make note of the differences in the registers being used compared to the registers that were initially used in the mov statements. 

Write out the original code register and any sub registers actually used. 

Explain why there were differences in the register in the code compared to any sub registers used and explain why. 

You can put this in any type of document you choose and submit it to Git. 
