
; this is how you comment something
; compile with the following for 32: 
; nasm -f elf64 -o three_mov_command_64.o three_mov_command_64.asm
;
; link with:
; ld -o three_mov_command_64 three_mov_command_64.o
;
; run with 
; ./three_mov_command_64




section .data 
	
section .bss 

section .text 

global _start 
   _start:
      mov rax, 42
      mov rbx, 42
      mov rcx, 42
      mov rdx, 42


      mov ax, 4
      ;mov rbx, ax ;won't work gives error
      ;mov rbx, word ax ' won't work gives error
      movzx rbx, word ax ; WORKS



;   ; exit gracefully
    mov eax, 1 
    mov ebx, 0
    int 80h ;; syscall 0x80 etc

