;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  
;;;   nasm -f elf64 -o thirteen_flags_64.o thirteen_flags_64.asm
;;;     
;;;   ld -o thirteen_flags_64  thirteen_flags_64.o
;;;    
;;;   http://www.c-jump.com/CIS77/ASM/Instructions/I77_0070_eflags_bits.htm
;;;   https://en.wikipedia.org/wiki/FLAGS_register


section .data
   msg db      "We probably won't need this ",0xa
section .bss
 
section .text

global  _start             
			                         
_start:   ; underscore so that the linker can find it by name 

   mov   rax, 1  ;1  x
   mov   rbx, 2  ;2  x
   mov   rcx, 3  ;3  x
   mov   rdx, 4  ;4  x
   ;; watch overflow and sign
   ;; i.e. infor registers eflags
   
   shr   rax, 1  ;5
   
   ;; parity flag; zero flag; auxiliary flag zero flag
   ;;Parity flag indicates whether the lowest order byte of the   result an arithmetic or bit wise operation has an even or odd number of 1s
	
   ;; zero flag: Set when arithmetic or bitshift operations produce a zero
In other words, this flag gets set if an arithmetic result is zero
   ;; carry flag: set when an arithmetic borrow or carry occurs 
   ;; Also set with some bitshift operations (such as when a bit falls of the end in a shr/shl)	
	
	
   shr   rax, 1 ;6
 
   
   shl   rbx, 15 ;7
   shl   rbx, 2  ;8

   ;; pushf and popf

   mov rax, 1  ;9
   
   ;; info registers eflags
   shr rax, 2  ;10
   
   ;; print /x $rax ;examine the rax register
    ;; info registers eflags
   ;; x /10xw  $sp  ; examing memory print out the next 10 values in hex in   word size of the 16 bit sp register
   
    pushf   ;11
    ;; x /10xw  $sp
    ;; info registers eflags

    mov rax, 42 ;12
    mov rbx, 42  ;13
    mov rcx, 42  ;14
    mov rdx, 42  ;15

   popf    ;16

								

   mov eax, 1 
   mov ebx, 0
   int 80h ;; syscall 0x80 etc
