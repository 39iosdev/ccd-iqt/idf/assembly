; Filename: Control.nasm
; Author:  39ios

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  
;;;   nasm -f elf64 -o fourteen_labels_64.o fourteen_labels_64.asm
;;;     
;;;   ld -o fourteen_labels_64  fourteen_labels_64.o
;
; Purpose: 

global _start			

section .text
_start:

	jmp Begin 

NeverExecute:

	mov eax, 0x10
	xor ebx, ebx

Begin:

    jmp .label1 
	

.label1:
    xor rax, rax 
    inc rax 
    mov rcx, rax 
    jmp .somelabel
    ;call .somelabel
    xor rax, rax
    call Done

.somelabel: 
    shl rcx, 3 
    xchg rcx, rax 
    ;ret ; won't work without the call, return address was 
    	;JMP doesn't set up the stack (by pushing the return value) so when you do a ret , a bogus return address gets popped off the stack and your code jumps to it.

Done:

	mov eax, 0x1
	mov ebx, 0xa		; sys_exit syscall
	int 0x80

section .data

	message: db "Hello World! ",0Ah
	mlen     equ $-message

