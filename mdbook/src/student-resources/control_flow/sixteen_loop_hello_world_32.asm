; Filename: Control.nasm
; Author:  39ios

; compile with the following for 32: 
; nasm -f elf sixteen_loop_hello_world_32.asm
;
; link with:
; ld -m elf_i386 sixteen_loop_hello_world_32.o -o sixteen_loop_hello_world_32
; run with 
; ./sixteen_loop_hello_world_32
;
;to debug use gdb after compiled and linked:
;
;gdb sixteen_loop_hello_world_32
;

global _start			

section .text
_start:

	jmp Begin

NeverExecute:

	mov eax, 0x10
	xor ebx, ebx

Begin:
	mov ecx, 0x5

PrintHW:

	push ecx
		
	; Print hello world using write syscall
	mov eax, 0x4
	mov ebx, 1
	mov ecx, message
	mov edx, mlen
	int 0x80

	pop ecx
	loop PrintHW

	mov eax, 0x1
	mov ebx, 0xa		; sys_exit syscall
	int 0x80

section .data

	message: db "Hello World! ",10
	mlen     equ $-message

