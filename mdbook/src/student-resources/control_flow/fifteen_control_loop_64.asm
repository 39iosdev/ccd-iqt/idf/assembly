; Filename: Control.nasm
; Author:  39ios

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  
;;;   nasm -f elf64 -o fifteen_control_loop_64.o fifteen_control_loop_64.asm
;;;     
;;;   ld -o fifteen_control_loop_64  fifteen_control_loop_64.o
;

SECTION .data
message: DB "Assembly is the best programming language!",0
len: equ ($ - message)

SECTION .text
global _start
_start:

    mov al, 0			; freq
    mov ecx, 0			; counter

top:	
   mov bl, [message+ecx]	; offset must be 32-bit
   or bl, 32			; lowercase
   cmp bl, 97			; compare with 'a'
   jne zloop
   inc al			; found an 'a'
zloop:
   inc ecx
   cmp ecx, len
   jne top

mov eax, 1
mov ebx, 0
int 80h