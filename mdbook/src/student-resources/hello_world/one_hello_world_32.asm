; this is how you comment something
; compile with the following for 32: 
; nasm -f elf one_hello_world_32.asm
;
; link with:
; ld -m elf_i386 one_hello_world_32.o -o one_hello_world_32
;
; run with 
; ./one_hello_world_32
;
;to debug use gdb after compiled and linked:
;
;gdb one_hello_world_32
;see the install_gdbinit.txt for more info


;to write to the console we need a sys call 
        ;SYS_WRITE has an opcode of 4 and takes 3 arguments
        ;the arguments are sequentially loaded into "EDX, ECX, EBX before the interrupt is requested
        ;EDX is loaded with the length in bytes of the string 
        ;ECX is loaded with the address of the variable in the data section to be used
        ;EBX is the file to write to or STDOUT 
        ;EAX holds the sys call to invoke 
        ; the other registers must be loaded before EAX
        ; man man to get the syscall section (should be 2);
        ;man 2 *syscallname* ex. man 2 write will give you the arguemts of write
        ;more info https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
        ; and here https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#x86_64-64_bit


; create data SECTION to hold initialized variables
SECTION .data
    msg db 'Hello World!', 0xa  ; 0xa is carriage return or use 0ah or 10
    len equ $-msg              ; $ means the current address according to the assembler. $ - msg is the current address of the assembler minus the address of msg, 
                               ; this gives us the length of the string in the msg 

; create text SECTION to hold the actual assembly code
SECTION .text         ; tells kernel where to begin execution
    global _start     ; tells where the program begins (to look for '_start')
    _start:           ; starting point of the program
        mov edx, len  ; num of bytes to write (argument 3 of sys_write)
        mov ecx, msg  ; mov memory address of msg string to ecx(argument 2 of sys_write)
        mov ebx, 1    ; write to STDOUT file (argument 1 of sys_write)
        mov eax, 4    ; invoke sys_write (kernel opcode 4)
        int 80h       ; kernel interrupt can also use 0x80

        mov ebx, 0    ; return status on exit of "no errors"
        mov eax, 1    ; invoke sys_exit (kernel opcode 1)
        int 80h       ; kernel interrupt can also use 0x80