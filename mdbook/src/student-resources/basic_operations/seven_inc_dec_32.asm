; this is how you comment something
; compile with the following for 32: 
; nasm -f elf seven_inc_dec_32.asm
;
; link with:
; ld -m elf_i386 seven_inc_dec_32.o -o seven_inc_dec_32
;
; run with 
; ./seven_inc_dec_32
;
;to debug use gdb after compiled and linked:
;
;gdb seven_inc_dec_32
;see the install_gdbinit.txt for more info


;to write to the console we need a sys call 
        ;SYS_WRITE has an opcode of 4 and takes 3 arguments
        ;the arguments are sequentially loaded into "EDX, ECX, EBX before the interrupt is requested
        ;EDX is loaded with the length in bytes of the string 
        ;ECX is loaded with the address of the variable in the data section to be used
        ;EBX is the file to write to or STDOUT 
        ;EAX holds the sys call to invoke 
        ; the other registers must be loaded before EAX
        ; man man to get the syscall section (should be 2);
        ;man 2 *syscallname* ex. man 2 write will give you the arguemts of write
        ;more info https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
        ; and here https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#x86_64-64_bit


; create data SECTION to hold initialized variables
SECTION .data
  

; create text SECTION to hold the actual assembly code
SECTION .text         ; tells kernel where to begin execution
    global _start     ; tells where the program begins (to look for '_start')
    _start:           ; starting point of the program
        mov eax, 90  ; mov first num into eax
        mov ebx, 9   ; mov second numbr into ebx
        sub eax, ebx ; subtract ebx to eax


        inc eax
        inc ebx
        
        mov edx, 5
        add eax, edx

        dec eax

       
    ;exit gracefully
       mov     ebx, 0
       mov     eax, 1
       int     80h
      
