; this is how you comment something
; compile with the following for 32: 
; nasm -f elf eleven_fun_stack_32.asm
;
; link with:
; ld -m elf_i386 eleven_fun_stack_32.o -o eleven_fun_stack_32
; run with 
; ./eleven_fun_stack_32
;
;to debug use gdb after compiled and linked:
;
;gdb televen_fun_stack_32
;
;see the install_gdbinit.txt for more info


;to write to the console we need a sys call 
        ;SYS_WRITE has an opcode of 4 and takes 3 arguments
        ;the arguments are sequentially loaded into "EDX, ECX, EBX before the interrupt is requested
        ;EDX is loaded with the length in bytes of the string 
        ;ECX is loaded with the address of the variable in the data section to be used
        ;EBX is the file to write to or STDOUT 
        ;EAX holds the sys call to invoke 
        ; the other registers must be loaded before EAX
        ; man man to get the syscall section (should be 2);
        ;man 2 *syscallname* ex. man 2 write will give you the arguemts of write
        ;more info https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
        ; and here https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#x86_64-64_bit


;Write a program that takes a normal 32-bit numeric value 
;(e.g., 0xFFFFh) and converts it to a byte array such that it 
;can be printed to the screen using a system call method. 
;A loop is necessary for converting the numeric value to ASCII for output. 
;Again, use a system call (e.g., int 80h) to print the value to the console 

; create data SECTION to hold initialized variables
SECTION .data
  val: DD 0xFFFF				; 65,535 of possible 4,294,967,295
  valArr: DB 0,0,0,0,0,0,0,0,0,0,0Ah,0	; 12 bytes
					; 10=max digits for 32-bit number
					; pre-load to 0 (non-print NULL)

; create text SECTION to hold the actual assembly code
SECTION .text         ; tells kernel where to begin execution
    global _start     ; tells where the program begins (to look for '_start')
    _start:           ; starting point of the program
        mov ecx, 9	; highest usable array index
	    mov ebx, 10	; number system base
	    mov eax, [val]	; initial value load for DIV

divide:
	xor edx, edx	; clear high bits of edx:eax for division
	div ebx
	or dl, 48	; remainder in edx is least sig digit,
			; not higher than 9, use dl+48 for ASCII

	mov BYTE [valArr+ecx], dl
	dec ecx

	cmp eax, 0	; if the quotient is zero, we are done
	jne divide

print:
	mov eax, 4	; syswrite
	mov ebx, 1	; stdout
	mov ecx, valArr	; starting address of string
	mov edx, 12	; len of string
	int 80h
	
mov eax, 1
mov ebx, 0
int 80h


       
    ;exit gracefully
       mov     ebx, 0
       mov     eax, 1
       int     80h
      
