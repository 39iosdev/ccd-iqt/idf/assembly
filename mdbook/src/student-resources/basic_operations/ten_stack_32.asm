; this is how you comment something
; compile with the following for 32: 
; nasm -f elf ten_stack_32.asm
;
; link with:
; ld -m elf_i386 ten_stack_32.o -o ten_stack_32
;
; run with 
; ./ten_stack_32
;
;to debug use gdb after compiled and linked:
;
;gdb ten_stack_32
;
;see the install_gdbinit.txt for more info


;to write to the console we need a sys call 
        ;SYS_WRITE has an opcode of 4 and takes 3 arguments
        ;the arguments are sequentially loaded into "EDX, ECX, EBX before the interrupt is requested
        ;EDX is loaded with the length in bytes of the string 
        ;ECX is loaded with the address of the variable in the data section to be used
        ;EBX is the file to write to or STDOUT 
        ;EAX holds the sys call to invoke 
        ; the other registers must be loaded before EAX
        ; man man to get the syscall section (should be 2);
        ;man 2 *syscallname* ex. man 2 write will give you the arguemts of write
        ;more info https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
        ; and here https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#x86_64-64_bit


; create data SECTION to hold initialized variables
SECTION .data
  sample:	db 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x11, 0x22

; create text SECTION to hold the actual assembly code
SECTION .text         ; tells kernel where to begin execution
    global _start     ; tells where the program begins (to look for '_start')
    _start:           ; starting point of the program
    mov eax, 0x66778899
	mov ebx, 0x0
	mov ecx, 0x0

	; push and pop of r/m16 and r/m32 

	; register push and pop 

	push ax
	pop bx

	push eax
	pop ecx

	;  memory push and pop 

	push word [sample]
	pop ecx

	push dword [sample]
	pop edx



       
    ;exit gracefully
       mov     ebx, 0
       mov     eax, 1
       int     80h
      
