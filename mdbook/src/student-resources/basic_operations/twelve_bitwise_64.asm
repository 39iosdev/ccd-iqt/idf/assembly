; this is how you comment something
; compile with the following for 32: 
; nasm -f elf64 -o twelve_bitwise_64.o twelve_bitwise_64.asm
;
; link with:
; ld -o twelve_bitwise_64 twelve_bitwise_64.o
;
; run with 
; ./twelve_bitwise_64


section .data
   msg db      "We probably won't need this ",0xa
section .bss

			



 
section .text

global  _start             
			                         
_start:   ; underscore so that the linker can find it by name 

 ;; cf movsx movzx 

   mov   bx, 42
   movsx ebx, bx 

   mov   rax, 1
   shl   rax, 1
   shl   rax, 3

   shr   rax, 1
   shr   rax, 3

   ;;; rol ral
   mov   rax, 1
   rol   rax, 1

   ror   rax, 1      
   ror   rax, 1 

    ;;; and / or 
    mov  rax, 1
    mov  rcx, 5 

   ;; checkout rax

    and  rax, rcx

    or   rax, rcx

    ;;; not 
    mov  rcx, 1
    not  rcx

    xor  rax, rax
    xor  rax, rcx
    xor  rcx, rax




								

   mov eax, 1 
   mov ebx, 0
   int 80h ;; syscall 0x80 etc
