# Ch02 Basic Operations

## Objectives
* Utilize basic arithmetic and bit operations
* Understand the difference between signed and unsigned values - *from an assembly perspective*
* Understand the ***Two's complement*** representation of signed numbers
* Understand and use the ***stack*** in assembly programming to write functions to load and store data

#### To access the Assembly Basic Operations slides please click [here](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/slides/#/)
