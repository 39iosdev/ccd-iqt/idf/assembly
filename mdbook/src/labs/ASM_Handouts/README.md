# The available handouts are available via the following links

* [ASM Cheatsheet](Assembly%20Cheat%20Sheet.pdf)
* [GDB Cheatsheet](GDB%20Cheat%20Sheet.pdf)
* [Lab instructions](ASM_Handouts/lab_instructions.txt)
* [Sample GDB init](ASM_Handouts/sample-gdbinit)