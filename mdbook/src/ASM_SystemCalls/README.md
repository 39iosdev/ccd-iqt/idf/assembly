# Chapter 4: System calls in assembly

## Objectives:

* Describe how to invoke system calls in Assembly
* Describe the purpose and how to use common system interrupts in Assembly
* Use interrupts to execute OS system calls
* Invoke system calls 
* Differentiate between **real** and **protected** mode 

---
