![](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/assets/asm_hello_world.png)

**Assembly** is a very low level language.
This module will introduce fundamental concepts of programming. 
These concepts are fundamental or *foundational* in that they are basically as close to machine language as a programmer can write.
In contrast to highly *abstracted* languages such as Python, Assembly can be very much architecture specific and provides a glimpse at the inner workings of the CPU.  

Assembly does little, in comparison to other programming languages, to hide the the core procedures and instructions used to control a computer.  

In this module, students will develop fluency in using core system resources - i.e. different registers, system calls, low level file access. 

This module is designed to help students:
- use the process stack and system registry
- understand and use:  
    `mov`, `movx`, `movzx`, `lea`, `xchg`, `push`, `pop`, `pushf`, `popf`, `call`, `ret`, `add`, `sub`, `mul`, `div`, `inc`, `dec`, `shl`, `shr`, `and`, `or`, `not`, `xor`, `rol`, `ror`, `sar`, `sal`, `cmp`, `test`, `jcc` (and other conditional jump instructions), `loop`, `scas`, `stos`, `lods`, `movs`, `cmps`, `rep`, `repne`, `repe`, `std`, and `cld` instructions.  
- create and utilize structures
- utilize flags, bit operations, labels, and pre-defined utility functions
- describe and use common interrupts and system calls
- use the GDB abd WinDBG debuggers to locate errors in programs
- understand how to allocate memory
- understand how to implement file handling
- understand different processor modes
- write Assembly code for different processor modes
- utilize the stack frame
- utilize registers and sub-registers
- implement branch instructions
- utilize x86 calling conventions
- utilize name mangling
- implement protection mechanisms
- execute instructions using raw opcodes
- implement byte ordering
- write assembly code that accomplishes the same functionality as a given high level code snippet


---  


**The Assembly lessons are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/)**

- Introduction to ASM [here](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html)

- ASM Basic Operations [here](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/index.html)

- ASM Control Flow [here](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/index.html)

---  

**Assembly Slides correspond to unit:**
 - [Introduction to ASM Slides](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/slides/#/)  

 - [ASM Basic Operations Slides](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/slides/#/)  

 - [ASM Control Flow Slides](https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/slides/#/)


---

**The Assembly Labs are located here:**

- Assembly Labs Folder [here](https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/tree/master/mdbook/src/labs)

 
