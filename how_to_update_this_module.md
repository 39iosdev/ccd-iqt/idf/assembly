# DEV CONTAINER SETUP WITH VS CODE

When opening this as a dev container, the same container used to build the pages will be pulled to your docker engine and it will create a small webserver that will auto update and build the changes you make to the MDbook for your review.  

To access this it would be:
```text
http://<ip address of docker host>:3000
```

## Windows

1. Setup a linux VM 
    - install docker on the linux VM
    - back to windows, install the docker cli (minimum) change the context of docker to point to the linux VM with docker
    - clone this repo and open to it as a base folder in vs code
    - a pop-up should show up asking if you want to open it in a wsl container.  Select that and wait for it to load

2. Setup WSL v2
    - install wsl container plugin for vscode
    - clone this repo and open to it as a base folder in vs code
    - a pop-up should show up asking if you want to open it in a wsl container.  Select that and wait for it to load

## Linux

1. Clone this repo to a folder
2. Ensure container and docker plugins are installed
3. run VS Code and load the repo as the base folder and look for container pop up
4. If the container pop-up does not show up, press F1 and type container and see if there is an option to rebuild project into a container.

## Codeblocks/Gitpod

Click the "Gitpod" button (sometimes hidden under the Web IDE dropdown).  
Note: currently it is not setup so that you would be able to build a test site to view the results of your changes 